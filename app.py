import os
import json
import requests
import urllib3
import math

from flask import Flask
from flask import Flask, render_template, request, jsonify

app = Flask(__name__, static_url_path='/static')

@app.route('/')
def Welcome():
	return render_template('index.htm')

@app.route('/score', methods=['GET', 'POST'])
def process_form_data():
	# Get the form data - result will contian all elements from the HTML form that was just submitted
	result = request.form

	### API_KEY

	apikey = "REPLACE_____WITH_____OWN_____API_____KEY"

	### OBTAIN TOKEN 

	# https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/ml-authentication.html?audience=wdp&context=cpdaas

	# Get an IAM token from IBM Cloud
	url     = "https://iam.bluemix.net/oidc/token"
	headers = { "Content-Type" : "application/x-www-form-urlencoded" }
	data    = "apikey=" + apikey + "&grant_type=urn:ibm:params:oauth:grant-type:apikey"
	IBM_cloud_IAM_uid = "bx"
	IBM_cloud_IAM_pwd = "bx"
	response  = requests.post( url, headers=headers, data=data, auth=( IBM_cloud_IAM_uid, IBM_cloud_IAM_pwd ) )
	iam_token = response.json()["access_token"]


	header = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + iam_token}


	payload_scoring = {"input_data": [{"fields": ["property_state", "distance_to_centre", "distance_to_metro", "mts2"], 
	"values": [[result["property_state"], result["distance_to_centre"], result["distance_to_metro"], result["mts2"]]]}] }

	response_scoring = requests.post('https://us-south.ml.cloud.ibm.com/ml/v4/deployments/6ff5a44d-0309-4053-a503-4611633994ce/predictions', json=payload_scoring, headers=header, params=[('version','2020-09-01')] )
	
	json_response = response_scoring.json()

	json_response['exp_values'] = math.exp( json_response['predictions'][0]['values'][0][0] ) 

	return jsonify( json_response ) 
	
port = os.getenv('VCAP_APP_PORT', '5000')
if __name__ == "__main__":
	app.run(host='0.0.0.0', port=int(port))
