# Example of WSC Deployment in Flask with IAM_token Authentication

- v4 of `wml`

Using the [IBM Cloud Foundry](https://cloud.ibm.com/docs/cloud-foundry-public/getting-started.html) as app foundation, this app will use a model in `wml` to score form data against.